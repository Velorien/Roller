﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;

namespace Roller
{
    public class MainViewModel : ViewModelBase
    {
        private ObservableCollection<RollResult> rolls;
        private string code;
        private string numberOfDice;

        private Randomizer randomizer = new Randomizer();

        public MainViewModel()
        {
            RollStandard = new RelayCommand(() => RollExecute(4, x => x.OrderByDescending(y => y).Take(3)));
            RollHeoric = new RelayCommand(() => RollExecute(2, x => { var list = new List<int>(x); list.Add(6); return list; }));
            RollClassic = new RelayCommand(() => RollExecute(3, x => x));
            RollEpic = new RelayCommand(() => RollExecute(5, x => x.OrderByDescending(y => y).Take(3)));
            RollCustom = new RelayCommand(RollCustomExecute);
            Code = "return x;";
            NumberOfDice = "5";
        }

        public RelayCommand RollStandard { get; private set; }

        public RelayCommand RollHeoric { get; private set; }

        public RelayCommand RollClassic { get; private set; }

        public RelayCommand RollEpic { get; private set; }

        public RelayCommand RollCustom { get; private set; }

        public ObservableCollection<RollResult> Rolls { get { return rolls; } set { Set(() => Rolls, ref rolls, value); } }

        public string Code { get { return code; } set { Set(() => Code, ref code, value); } }

        public string NumberOfDice { get { return numberOfDice; } set { Set(() => NumberOfDice, ref numberOfDice, value); } }

        private void RollExecute(int dice, Func<IList<int>, IEnumerable<int>> method)
        {
            var results = new List<RollResult>(6);

            for (int i = 0; i < 6; i++)
            {
                var temp = new List<int>();
                for (int j = 0; j < dice; j++)
                {
                    temp.Add(randomizer.Roll(1, 6));
                }

                results.Add(new RollResult(method) { Rolls = temp });
            }

            Rolls = new ObservableCollection<RollResult>(results);
        }

        private void RollCustomExecute()
        {
            try
            {
                var method = DynamicRoller.CreateMethod(Code);
                RollExecute(int.Parse(NumberOfDice), method.CreateDelegate(typeof(Func<IList<int>, IEnumerable<int>>)) as Func<IList<int>, IEnumerable<int>>);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
